
#compile each lesson one by one
#copy results to the one directory
mkdir -p result

#Libraries needed:
# GL, GLU, SDL
#
# Some lessons also need:
# GLUT = 26 27 39
# SDL_mixer = 21 32


for I in 01 02 03 04 05 06 07 08 09 10 11 12 13 \
         16 17 18 19 20 22 23 24 25 28 \
         29 30 31 33 34 36 37 40 41 42 43 \
         45 48 \
         21 32 \
         26 27 39

do
	cd lesson$I
	make clean
	make
	cp lesson$I ../result
	if [ -d data ] ; then
		cp -r data ../result
	fi
	cd ..
done


