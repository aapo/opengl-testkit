echo "missing ones: 14 15 38 44 46"


#'linux'
for I in 26 27 35 39
do
	wget --no-verbose http://nehe.gamedev.net/data/lessons/linux/lesson$I.tar.gz
	if [ -f lesson$I.tar.gz ]
		then
		tar xf lesson$I.tar.gz
		fi
done

#'linuxglx'
for I in 28 29 30 31 33 34 47 48
do
	wget --no-verbose http://nehe.gamedev.net/data/lessons/linuxglx/lesson$I.tar.gz
	if [ -f lesson$I.tar.gz ]
		then
		tar xf lesson$I.tar.gz
		fi
done

#'linuxsdl'
for I in 01 02 03 04 05 06 07 08 09 10 11 12 13 16 17 18 19 20 21 22 23 24 \
         25 32 36 37 40 41 42 43 45 
do
	wget --no-verbose http://nehe.gamedev.net/data/lessons/linuxsdl/lesson$I.tar.gz
	if [ -f lesson$I.tar.gz ]
		then
		tar xf lesson$I.tar.gz
		fi
done


